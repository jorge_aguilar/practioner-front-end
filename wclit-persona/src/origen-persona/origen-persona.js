import { LitElement, html, css } from 'lit-element';

export class OrigenPersona  extends LitElement {

  static get styles() {
    return css`
      select {
        width: 200px;
      }
    `;
  }

  static get properties() {
    return {
        origenes: {type: Array}
    };
  }

  constructor() {
    super();
    this.origenes = ["México","USA","Canada"];
  }

  render() {
    return html`
        <label>Origen</label>
        <select id="sel" @change="${this.selChange}">
            ${this.origenes.map(i => html`<option>${i}</option>`)}
        </select>
    `;
  }

  selChange(e){
      let valor = this.shadowRoot.querySelector("#sel").value;
      let event = new CustomEvent('origen-set',
                                    { detail: { message: valor}});
      this.dispatchEvent(event);                                    
  }
}

customElements.define('origen-persona', OrigenPersona);