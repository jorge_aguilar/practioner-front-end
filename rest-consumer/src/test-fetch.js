import { LitElement, html, css } from 'lit-element';

class TestFetch  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        planet: {type:Object}
    };
  }

  constructor() {
    super();
    this.planet = { results: []}
  }

  render() {
    return html`
        ${this.planet.results.map((pl) => {
            return html `<div>${pl.name} ${pl.rotation_period}</div>`
        })}
      
    `;
  }
  connectedCallback() {
      super.connectedCallback();
      try {
          this.cargarPlanetas();
      } catch (error) {
          alert(error);
      }
  }
  cargarPlanetas(){
      fetch("https://swapi.dev/api/planets/")
        .then(response => {
            console.log(response);
            if (!response.ok) { throw response; }
            return response.json();
        })
        .then(data => {
            this.planet = data;
            console.log(data);
        })
        .catch(error => {
            alert("Problemas con el fetch: " +error);
        })
  }
}

customElements.define('test-fetch', TestFetch);