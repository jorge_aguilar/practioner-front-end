import { LitElement, html, css } from "lit-element";

class BookForm extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        border: 1px solid purple;
        margin: 2em;
        padding: 2em;
      }
      input {
        margin-bottom: 1em;
      }
    `;
  }

  static get properties() {
    return {
      id: { type: Number },
      titulo: { type: String },
      autor: { type: String },
    };
  }

  constructor() {
    super();
    this.titulo = "";
    this.autor = "";
  }
  render() {
    return html`
      <div>
        <label for="book-id">ID</label>
        <input
          type="number"
          id="book-id"
          .value="${this.id}"
          @input="${this.updateId}"
        />
        <br />
        <label for="book-title">Titulo</label>
        <input
          type="text"
          id="book-title"
          .value="${this.titulo}"
          @input="${this.updateTitulo}"
        />
        <br />
        <label for="book-autor">Autor</label>
        <input
          type="text"
          id="book-autor"
          .value="${this.autor}"
          @input="${this.udpateAutor}"
        />
        <br />
        <button @click="${this.buscarBook}">Buscar</button>
        <button @click="${this.crearBook}">Crear</button>
        <button @click="${this.modificarBook}">Modificar</button>
        <button @click="${this.eliminarBook}">Eliminar</button>
      </div>
    `;
  }

  updateId(e) {
    this.id = +e.target.value;
  }

  updateTitulo(e) {
    this.titulo = e.target.value;
  }

  udpateAutor(e) {
    this.autor = e.target.value;
  }

  buscarBook() {
    fetch(`http://localhost:3392/books/${this.id}`)
      .then((res) => {
        if (!res.ok) throw res;
        return res.json();
      })
      .then((data) => {
        this.id = data.id;
        this.titulo = data.titulo;
        this.autor = data.autor;
      })
      .catch((err) => alert(err));
  }

  getCurrentBook() {
    const book = {
      id: this.id,
      titulo: this.titulo,
      autor: this.autor,
    };
    return book;
  }

  crearBook() {
    const book = this.getCurrentBook();
    const options = {
      method: "POST",
      body: JSON.stringify(book),
      headers: {
        "Content-Type": "application/json",
      },
    };

    fetch("http://localhost:3392/books/", options)
      .then((res) => {
        if (!res.ok) throw res;
        return res.json();
      })
      .then((data) => {
        // this.data = data;
        // console.log(data);
        alert("Libro creado!");
        location.reload();
      })
      .catch((err) => alert(err));
  }

  modificarBook() {
    const book = this.getCurrentBook();
    const options = {
      method: "PUT",
      body: JSON.stringify(book),
      headers: {
        "Content-Type": "application/json",
      },
    };

    fetch("http://localhost:3392/books/", options)
      .then((res) => {
        if (!res.ok) throw res;
        return res.json();
      })
      .then((data) => {
        // this.data = data;
        // console.log(data);
        alert("Libro Modificado!");
        location.reload();
      })
      .catch((err) => alert(err));
  }

  eliminarBook() {
    const book = this.getCurrentBook();
    const options = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };

    fetch(`http://localhost:3392/books/${this.id}`, options)
      .then((res) => {
        if (!res.ok) throw res;
        return res.json();
      })
      .then((data) => {
        // this.data = data;
        // console.log(data);
        alert("Libro Eliminado!");
        location.reload();
      })
      .catch((err) => alert(err));
  }
}
customElements.define("book-form", BookForm);
